
/*
 * Yi Tan
 * stn: 991554680
 */



package passwordValidator;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {
	
	

	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("aAdsd8"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("aA"));
	}

	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("787899"));
	}

	@Test
	public void testHasValidCaseCharsExceptionSpecial() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("!#%"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("ABCDE"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("abcded"));
	}


	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.IsValidLength("1234567890");
		assertTrue("Invalid Length", result);
		
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.IsValidLength("");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean result = PasswordValidator.IsValidLength("  test  ");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.IsValidLength("12345678");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.IsValidLength("1234567");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsRegular() {
		boolean result = PasswordValidator.IsValidLength("12345678910");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsException() {
		boolean result = PasswordValidator.IsValidLength("");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryIn() {
		boolean result = PasswordValidator.IsValidLength("abcdefghg12");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryOut() {
		boolean result = PasswordValidator.IsValidLength("abcdefghg1");
		assertTrue("Invalid Length", result);
	}

	

}
	

